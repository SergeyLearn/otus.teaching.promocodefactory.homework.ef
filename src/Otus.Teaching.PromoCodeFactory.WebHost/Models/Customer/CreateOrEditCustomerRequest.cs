﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer
{
    public class CreateOrEditCustomerRequest
    {
        
        [Required]
        [MinLength(1)]
        public string FirstName { get; set; }
        
        [Required]
        [MinLength(1)]
        public string LastName { get; set; }
        
        [Required]
        [MinLength(1)]
        public string Email { get; set; }
        public List<Guid> PreferenceIds { get; set; }

        public Core.Domain.PromoCodeManagement.Customer CreateCustomer()
        {
            var id = Guid.NewGuid();
            return new Core.Domain.PromoCodeManagement.Customer
            {
                Id = id,
                FirstName = FirstName,
                LastName = LastName,
                Email = Email,
                CustomerPreferences = PreferenceIds?.Select(p => new CustomerPreference { CustomerId  = id, PreferenceId = p}).ToList() ?? new List<CustomerPreference>()
            };
        }

        public static Core.Domain.PromoCodeManagement.Customer UpdateCustomer(Core.Domain.PromoCodeManagement.Customer customer, CreateOrEditCustomerRequest request)
        {
            var customerNew = request.CreateCustomer();

            customer.Email = customerNew.Email;
            customer.FirstName = customerNew.FirstName;
            customer.LastName = customerNew.LastName;
            customer.CustomerPreferences = customerNew.CustomerPreferences;

            return customer;
        }
    }
}