// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public sealed class DataContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
            // Database.EnsureDeleted();
            Database.EnsureCreated();
        }
        
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().Property(r => r.Name).IsRequired().HasMaxLength(64);
            modelBuilder.Entity<Role>().Property(r => r.Description).IsRequired().HasMaxLength(1024);
            
            modelBuilder.Entity<Employee>().Property(r => r.Email).IsRequired().HasMaxLength(64);
            modelBuilder.Entity<Employee>().Property(r => r.FirstName).IsRequired().HasMaxLength(64);
            modelBuilder.Entity<Employee>().Property(r => r.LastName).IsRequired().HasMaxLength(64);
            modelBuilder.Entity<Employee>()
                .HasOne(e => e.Role)
                .WithMany(r => r.Employees)
                .HasForeignKey(e => e.RoleId);
            
            modelBuilder.Entity<Customer>().Property(c => c.Email).IsRequired().HasMaxLength(64);
            modelBuilder.Entity<Customer>().Property(c => c.FirstName).IsRequired().HasMaxLength(64);
            modelBuilder.Entity<Customer>().Property(c => c.LastName).IsRequired().HasMaxLength(64);
            modelBuilder.Entity<Customer>()
                .HasMany(p => p.PromoCodes)
                .WithOne(c => c.Customer)
                .HasForeignKey(pc => pc.CustomerId)
                .OnDelete(DeleteBehavior.Cascade);
            
            modelBuilder.Entity<CustomerPreference>()
                .HasKey(сp => new {сp.CustomerId, сp.PreferenceId});
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(c => c.Customer)
                .WithMany(b => b.CustomerPreferences)
                .HasForeignKey(bc => bc.CustomerId);
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(c => c.Preference)
                .WithMany(b => b.CustomerPreferences)
                .HasForeignKey(bc => bc.PreferenceId);
            
            modelBuilder.Entity<Preference>().Property(p => p.Name).IsRequired().HasMaxLength(64);
            
            modelBuilder.Entity<PromoCode>().Property(p => p.Code).IsRequired().HasMaxLength(1024);
            modelBuilder.Entity<PromoCode>().Property(p => p.PartnerName).IsRequired().HasMaxLength(64);
            modelBuilder.Entity<PromoCode>().Property(p => p.ServiceInfo).IsRequired().HasMaxLength(1024);
            modelBuilder.Entity<PromoCode>().Property(p => p.BeginDate).IsRequired();
            modelBuilder.Entity<PromoCode>().Property(p => p.EndDate).IsRequired();

            modelBuilder.Seed();
        }


    }
}