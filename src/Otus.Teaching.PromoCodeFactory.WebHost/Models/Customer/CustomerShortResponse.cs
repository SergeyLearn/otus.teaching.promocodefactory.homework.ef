﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer
{
    public class CustomerShortResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        
        
        public CustomerShortResponse (Core.Domain.PromoCodeManagement.Customer customer)
        {
            Id = customer.Id;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Email = customer.Email;
        }
    }
}