﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _repository;

        public CustomersController(IRepository<Customer> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Получение всех потребителей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync() 
            => Ok((await _repository.GetAllAsync()).Select(c => new CustomerShortResponse(c)).ToList());

        /// <summary>
        /// Получение потребителя по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Status OK or NotFound</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _repository.GetByIdAsync(id);
            return customer == null ? (ActionResult<CustomerResponse>) NotFound() : Ok(new CustomerResponse(customer));
        }

        /// <summary>
        /// Добавление потребителя
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync([FromBody][Required]CreateOrEditCustomerRequest request)
        {
            Customer customer = request.CreateCustomer();
            await _repository.AddAsync(customer);
            return Ok();
        }
        
        /// <summary>
        /// Редактирование данных потребителя
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns>Status OK or NotFound</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync([Required]Guid id, [FromBody][Required]CreateOrEditCustomerRequest request)
        {
            var customer = await _repository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();
            
            customer = CreateOrEditCustomerRequest.UpdateCustomer(customer, request);
            await _repository.UpdateAsync(customer);
            return Ok();
        }
        
        /// <summary>
        /// Удаление потребителя
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Status OK or NotFound</returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer([Required]Guid id)
        {
            var customer = await _repository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();
            
            await _repository.DeleteAsync(customer);
            return Ok();
        }
    }
}